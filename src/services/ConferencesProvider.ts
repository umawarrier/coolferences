import { getConfiguration } from "../config";
import { ConferenceJson } from "../entities/api/ConferenceJson";
import { ResponseJson } from "../entities/api/ResponseJson";
import { Conference } from "../entities/Conference";
import { ConferenceFactory } from "../entities/factories";

interface GetOptions {
  getter: (url: string) => Promise<Response>;
}

export class ConferencesProvider {
  public static async get({ getter }: GetOptions): Promise<Conference[]> {
    const { api } = getConfiguration();
    const response = await getter(api);

    if (!response.ok) {
      throw new Error(response.statusText);
    }

    const json = await response.json();

    return this.parseJson(json);
  }

  private static parseJson(json: ResponseJson): Conference[] {
    return Object.values(json)
      .map((conferences: ConferenceJson[]) => {
        return conferences.map((conference: ConferenceJson) => ConferenceFactory(conference));
      })
      .reduce((result: Conference[], element: Conference[]) => result.concat(element), [])
      .reverse();
  }
}
