export interface LocationJson {
  city: string;
  country: string;
  emoji_country: string;
}
