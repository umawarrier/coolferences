import React, { Fragment } from "react";

import { Conferences } from "../../components/Conferences";
import { Loader } from "../../components/Loader";
import { SearchBox } from "../../components/SearchBox";
import { getConfiguration } from "../../config";
import { Conference as ConferenceEntity } from "../../entities/Conference";
import { ConferencesProvider } from "../../services/ConferencesProvider";

import "./style.css";

interface ConferencesContainerState {
  conferences: ConferenceEntity[] | null;
  loading: boolean;
}

export class ConferencesContainer extends React.Component<{}, ConferencesContainerState> {
  public static CUSTOM_DELAY_BEFORE_RENDER = 2000;

  constructor(props: {}) {
    super(props);

    this.state = { conferences: null, loading: true };

    this.onSearch = this.onSearch.bind(this);
  }

  public async componentDidMount() {
    setTimeout(() => {
      this.setState({ loading: false });
    }, ConferencesContainer.CUSTOM_DELAY_BEFORE_RENDER);

    const conferences = await ConferencesProvider.get({ getter: fetch });

    this.setState({ conferences });
  }

  public render() {
    if (this.state.loading || !this.state.conferences) {
      return <Loader phrase="We're getting everything ready 🚀" />;
    }

    return (
      <Fragment>
        {getConfiguration().featureFlags.searchConferences && (
          <SearchBox
            className="ConferencesSearch"
            onChange={this.onSearch}
            placeholder="Search for a conference..."
          />
        )}

        <Conferences conferences={this.state.conferences} />
      </Fragment>
    );
  }

  private onSearch(_: string) {
    // TODO
  }
}
